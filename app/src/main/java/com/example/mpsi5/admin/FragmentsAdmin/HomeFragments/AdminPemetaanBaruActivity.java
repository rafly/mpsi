package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.AdminMainActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AdminPemetaanBaruActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    EditText fuNamaTempat, fuLokasi, fuLong, fuLat, jumlahAnggota;
    Button uploadBangunan;
    ImageButton btBack, gambar;
    private Uri imageUri;
    private String currentUserUID;
    //    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    final private StorageReference storageReference = FirebaseStorage.getInstance().getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_pemetaan_baru);

        fuNamaTempat = findViewById(R.id.tietNamaTempat);
        fuLokasi = findViewById(R.id.tietLokasi);
        fuLong = findViewById(R.id.tietLong);
        fuLat = findViewById(R.id.tietLat);
        jumlahAnggota = findViewById(R.id.tietPengurus);
        uploadBangunan = findViewById(R.id.buttonUploadBangunan);
        gambar = findViewById(R.id.imageButtonGambar);
        btBack = findViewById(R.id.btBack);

// firebase init
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        if (user != null) {
            currentUserUID = user.getUid();
        } else {
            // Handle the case when the user is not authenticated.
        }

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            imageUri = data.getData();
                            gambar.setImageURI(imageUri);
                        } else {
                            Toast.makeText(AdminPemetaanBaruActivity.this, "No Image Selected", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPicker = new Intent();
                photoPicker.setAction(Intent.ACTION_GET_CONTENT);
                photoPicker.setType("image/*");
                activityResultLauncher.launch(photoPicker);
            }
        });

        uploadBangunan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageUri != null && !TextUtils.isEmpty(fuNamaTempat.getText()) &&
                        !TextUtils.isEmpty(fuLokasi.getText()) && !TextUtils.isEmpty(fuLong.getText()) && !TextUtils.isEmpty(fuLat.getText()) &&
                        !TextUtils.isEmpty(jumlahAnggota.getText())) {
                    uploadToFirebase(imageUri,fuNamaTempat,fuLokasi,fuLong,fuLat,jumlahAnggota);
                } else {
                    Toast.makeText(AdminPemetaanBaruActivity.this, "Isi semua data untuk bisa mengunggah bangunan!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void uploadToFirebase(Uri uri, EditText fuNamaTempat, EditText fuLokasi, EditText fuLong, EditText fuLat, EditText jumlahAnggota) {
        String namaTempat = fuNamaTempat.getText().toString();
        String lokasi = fuLokasi.getText().toString();
        String kLat = fuLat.getText().toString();
        String kLong = fuLong.getText().toString();
        String pJumlahAnggota = jumlahAnggota.getText().toString();

        String uniqueFileName = System.currentTimeMillis() + "." + getFileExtension(uri);
        final StorageReference imageReference = storageReference.child(namaTempat).child(uniqueFileName);

        // Check if the file with the same name already exists
        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // If onSuccess is called, the file with the same name already exists
                // Handle the case here, you may want to show an error message or take appropriate action
                Toast.makeText(AdminPemetaanBaruActivity.this, "File with the same name already exists", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                // If onFailure is called, the file doesn't exist, so you can proceed with the upload
                imageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                DataFasilitasClass dataClass = new DataFasilitasClass(uri.toString(), namaTempat, lokasi, kLong, kLat, pJumlahAnggota, uniqueFileName);
                                String key = databaseReference.child("Data").child("Fasilitas").push().getKey();
                                databaseReference.child("Data").child("Pemetaan").child(key).setValue(dataClass);
                                Toast.makeText(AdminPemetaanBaruActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(AdminPemetaanBaruActivity.this, AdminMainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AdminPemetaanBaruActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private String getFileExtension(Uri fileUri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(fileUri));
    }
}