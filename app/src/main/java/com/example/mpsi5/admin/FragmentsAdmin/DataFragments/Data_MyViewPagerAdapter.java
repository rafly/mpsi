package com.example.mpsi5.admin.FragmentsAdmin.DataFragments;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.Data_FragmentAdmin.BlmTerferivikasiFragment;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.Data_FragmentAdmin.TerferivikasiFragment_masihSalah;

public class Data_MyViewPagerAdapter extends FragmentStateAdapter {
    public Data_MyViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new TerferivikasiFragment_masihSalah();
            case 1:
                return new BlmTerferivikasiFragment();
            default:
                return new TerferivikasiFragment_masihSalah();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
