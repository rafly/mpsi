package com.example.mpsi5.admin;

import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.AnnounFragment;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.DataFragment;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.HomeFragment;
import com.example.mpsi5.admin.FragmentsAdmin.ProfileFragment;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class AdminMainActivity extends AppCompatActivity {

    MeowBottomNavigation buttonNavigatiion;
    RelativeLayout main_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);

        main_layout = findViewById(R.id.main_layout);

        buttonNavigatiion = findViewById(R.id.bottomNavigation);

        replace(new HomeFragment());

        buttonNavigatiion.show(1, true);

        buttonNavigatiion.add(new MeowBottomNavigation.Model(1, R.drawable.baseline_home_24));
//        buttonNavigatiion.add(new MeowBottomNavigation.Model(2, R.drawable.baseline_announcement_24));
        buttonNavigatiion.add(new MeowBottomNavigation.Model(2, R.drawable.baseline_data_24));
        buttonNavigatiion.add(new MeowBottomNavigation.Model(3, R.drawable.baseline_person_black_24));

        meownavigation();
//        main_layout.setBackgroundColor(Color.parseColor("#FF5722"));
    }

    private void meownavigation(){
        buttonNavigatiion.setOnClickMenuListener(new Function1<MeowBottomNavigation.Model, Unit>() {
            @Override
            public Unit invoke(MeowBottomNavigation.Model model) {

                switch (model.getId()){
                    case 1:
                        replace(new HomeFragment());
//                        main_layout.setBackgroundColor(Color.parseColor("#FF5722"));
                        break;

//                    case 2:
//                        replace(new AnnounFragment());
////                        main_layout.setBackgroundColor(Color.parseColor("#FF57"));
//                        break;

                    case 2:
                        replace(new DataFragment());
//                        main_layout.setBackgroundColor(Color.parseColor("#FF5"));
                        break;

                    case 3:
                        replace(new ProfileFragment());
//                        main_layout.setBackgroundColor(Color.parseColor("#F0F0F0"));
                        break;
                }

                return null;
            }
        });
    }

    private void replace (Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();
    }
}