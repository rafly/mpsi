package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Home_FragmetAdminMain;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.DataFasilitasClass;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.AdminFUBaruActivity;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.FURecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FasilitasUmumFragment extends Fragment {
    FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;
    private ArrayList<DataFasilitasClass> dataList;
    private FURecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fasilitas_umum, container, false);

        //recycler
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//
        dataList = new ArrayList<>();
        adapter = new FURecyclerAdapter(requireActivity(), dataList);//
        recyclerView.setAdapter(adapter);

        DatabaseReference imagesRef = FirebaseDatabase.getInstance().getReference("Data").child("Fasilitas");

        // Mendengarkan perubahan di direktori Images dan mengisi dataList
        imagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dataList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataFasilitasClass dataClass = dataSnapshot.getValue(DataFasilitasClass.class);
                    dataClass.setId(dataSnapshot.getKey());
                    dataList.add(dataClass);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle any errors if necessary
            }
        });

        floatingActionButton = view.findViewById(R.id.floatingActionButton);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdminFUBaruActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}