package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mpsi5.R;

import java.util.ArrayList;

public class FURecyclerAdapter extends RecyclerView.Adapter<FURecyclerAdapter.MyViewHolder> {

    private ArrayList<DataFasilitasClass> dataList;
    private ArrayList<DataFasilitasClass> filteredDataList;
    private Context context;
    final String tag ="FURecyclerAdapter";

    public void setFilteredList(ArrayList<DataFasilitasClass> filteredList) {
        filteredDataList = new ArrayList<>(filteredList);
        notifyDataSetChanged();
    }
    public FURecyclerAdapter(Context context, ArrayList<DataFasilitasClass> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fu_recycler_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Glide.with(context).load(dataList.get(position).getImageUrl()).into(holder.recyclerImage);
        holder.recyclerNamaTempat.setText(dataList.get(position).getNamaTempat());
        holder.recyclerLokasi.setText(dataList.get(position).getLokasi());

        holder.layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Menggunakan Intent untuk berpindah ke Info_FU.java
                Intent intent = new Intent(context, Info_FU.class);

                // Mengirim data tambahan jika diperlukan
                intent.putExtra("tempatId", dataList.get(position).getId());
                Log.d(tag, "tempatId" + intent);

                // Memulai aktivitas baru
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView recyclerImage;
        TextView recyclerNamaTempat, recyclerLokasi;
        ConstraintLayout layout1;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerImage = itemView.findViewById(R.id.imageView);
            recyclerNamaTempat = itemView.findViewById(R.id.tvNamaTempat);
            recyclerLokasi = itemView.findViewById(R.id.tvLokasi);
            layout1 = itemView.findViewById(R.id.layout1);
            layout1.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }
}
