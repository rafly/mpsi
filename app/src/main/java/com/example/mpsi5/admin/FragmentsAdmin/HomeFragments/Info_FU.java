package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.example.mpsi5.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Info_FU extends AppCompatActivity {

    Button btOpenMaps;
    ImageButton btBack, btGambar;
    TextView textNamaTempat, textJenisBagunan, textLokasi, textKoordinat, textJamOperasional, textNomor, textPengurus;
    String tempatId;
    DatabaseReference databaseReference;
    final String tag = "Info_FU";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_fu);

        btOpenMaps = findViewById(R.id.btOpenMaps);
        btBack = findViewById(R.id.btBack);
        btGambar = findViewById(R.id.ifuImageButton);
        textNamaTempat = findViewById(R.id.ifuNamaTempat);
        textJenisBagunan = findViewById(R.id.ifuJenisBagunan);
        textLokasi = findViewById(R.id.ifuAlamat);
        textKoordinat = findViewById(R.id.ifuKoordinat);
        textJamOperasional = findViewById(R.id.ifuJamOperasional);
        textNomor = findViewById(R.id.ifuNomor);
        textPengurus = findViewById(R.id.ifuPengurus);

        databaseReference = FirebaseDatabase.getInstance().getReference("Data").child("Fasilitas");

        Intent intent = getIntent();
        tempatId = intent.getStringExtra("tempatId");

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        databaseReference.child(tempatId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String namaTempat = dataSnapshot.child("namaTempat").getValue(String.class);
                    String jenisBangunan = dataSnapshot.child("jenisBangunan").getValue(String.class);
                    String lokasi = dataSnapshot.child("lokasi").getValue(String.class);
                    String kLong = dataSnapshot.child("kLong").getValue(String.class);
                    String kLat = dataSnapshot.child("kLat").getValue(String.class);
                    String jamOperasional = dataSnapshot.child("jamOperasional").getValue(String.class);
                    String nomor = dataSnapshot.child("kontak").getValue(String.class);
                    String pengurus = dataSnapshot.child("pengurus").getValue(String.class);
                    String imageUrl = dataSnapshot.child("imageUrl").getValue(String.class);

                    textNamaTempat.setText(namaTempat);
                    textJenisBagunan.setText(jenisBangunan);
                    textLokasi.setText(lokasi);
                    textKoordinat.setText(kLat + ", " + kLong);
                    textJamOperasional.setText(jamOperasional);
                    textNomor.setText("+62 "+ nomor);
                    textPengurus.setText(pengurus);
//                    RequestOptions requestOptions = new RequestOptions().placeholder(android.R.drawable.ic_launcher_foreground);
                    Glide.with(Info_FU.this)
                            .load(imageUrl)
                            .centerCrop()
//                            .apply(requestOptions)
//                            .transition(DrawableTransitionOptions.withCrossFade())
                            .into(btGambar);

                    btOpenMaps.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Mendapatkan nilai latitude dan longitude dari data Firebase
                            double latitude = Double.parseDouble(kLat);
                            double longitude = Double.parseDouble(kLong);

                            // Membuat Uri untuk membuka Google Maps dengan koordinat yang diinginkan
                            String uri = "geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude + "(Label+Location)";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            intent.setPackage("com.google.android.apps.maps");

                            // Memeriksa apakah Google Maps terinstal di perangkat
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                            } else {
                                // Jika Google Maps tidak terinstal, Anda bisa memberikan pesan kepada pengguna
                                Log.e(tag, "Google Maps not installed");
                                // Atau, Anda bisa menavigasikan pengguna untuk mengunduh Google Maps
                                // dengan membuka halaman Google Maps di Google Play Store
                                // Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
                                // startActivity(browserIntent);
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(tag, "Error retrieving data: " + databaseError.getMessage());
            }
        });
    }
}