package com.example.mpsi5.admin.FragmentsAdmin.DataFragments;

public class DataWargaClass {
    String imageURL, statusAkun, NIK, namaLengkap, TTL, jenisKelamin, agama, relasi, id;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStatusAkun() {
        return statusAkun;
    }

    public void setStatusAkun(String statusAkun) {
        this.statusAkun = statusAkun;
    }

    public String getNIK() {
        return NIK;
    }

    public void setNIK(String NIK) {
        this.NIK = NIK;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getTTL() {
        return TTL;
    }

    public void setTTL(String TTL) {
        this.TTL = TTL;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getRelasi() {
        return relasi;
    }

    public void setRelasi(String relasi) {
        this.relasi = relasi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DataWargaClass(String id, String imageURL, String namaLengkap, String TTL, String NIK, String jenisKelamin, String agama, String relasi, String statusAkun) {
        this.id = id;
        this.NIK = NIK;
        this.namaLengkap = namaLengkap;
        this.TTL = TTL;
        this.jenisKelamin = jenisKelamin;
        this.agama = agama;
        this.relasi = relasi;
        this.imageURL = imageURL;
        this.statusAkun = statusAkun;
    }

    public DataWargaClass(String imageURL, String namaLengkap, String TTL, String NIK, String jenisKelamin, String agama, String relasi, String statusAkun) {
        this.NIK = NIK;
        this.namaLengkap = namaLengkap;
        this.TTL = TTL;
        this.jenisKelamin = jenisKelamin;
        this.agama = agama;
        this.relasi = relasi;
        this.imageURL = imageURL;
        this.statusAkun = statusAkun;
    }
}
