package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

public class DataFasilitasClass {
    private String imageUrl, namaTempat, jamOperasional, jenisBangunan, lokasi, kLong, kLat, pengurus, kontak, imageName, jumlahAnggota, id;

    public DataFasilitasClass(){

    }

    public String getJenisBangunan() {
        return jenisBangunan;
    }

    public void setJenisBangunan(String jenisBangunan) {
        this.jenisBangunan = jenisBangunan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJumlahAnggota() {
        return jumlahAnggota;
    }

    public void setJumlahAnggota(String jumlahAnggota) {
        this.jumlahAnggota = jumlahAnggota;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNamaTempat() {
        return namaTempat;
    }

    public void setNamaTempat(String namaTempat) {
        this.namaTempat = namaTempat;
    }

    public String getJamOperasional() {
        return jamOperasional;
    }

    public void setJamOperasional(String jamOperasional) {
        this.jamOperasional = jamOperasional;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getkLong() {
        return kLong;
    }

    public void setkLong(String kLong) {
        this.kLong = kLong;
    }

    public String getkLat() {
        return kLat;
    }

    public void setkLat(String kLat) {
        this.kLat = kLat;
    }

    public String getPengurus() {
        return pengurus;
    }

    public void setPengurus(String pengurus) {
        this.pengurus = pengurus;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public DataFasilitasClass(String imageUrl, String namaTempat, String jamOperasional, String jenisBangunan, String lokasi, String kLong, String kLat, String pengurus, String kontak, String imageName) {
        this.imageUrl = imageUrl;
        this.namaTempat = namaTempat;
        this.jamOperasional = jamOperasional;
        this.jenisBangunan = jenisBangunan;
        this.lokasi = lokasi;
        this.kLong = kLong;
        this.kLat = kLat;
        this.pengurus = pengurus;
        this.kontak = kontak;
        this.imageName = imageName;
    }

    public DataFasilitasClass(String imageUrl, String namaTempat, String lokasi, String kLong, String kLat, String jumlahAnggota, String imageName) {
        this.imageUrl = imageUrl;
        this.namaTempat = namaTempat;
        this.lokasi = lokasi;
        this.kLong = kLong;
        this.kLat = kLat;
        this.jumlahAnggota = jumlahAnggota;
        this.imageName = imageName;
    }
}
