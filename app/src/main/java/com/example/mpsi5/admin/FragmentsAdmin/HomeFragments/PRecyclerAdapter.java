package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mpsi5.R;

import java.util.ArrayList;

public class PRecyclerAdapter extends RecyclerView.Adapter<PRecyclerAdapter.PMyViewHolder> {

    private ArrayList<DataFasilitasClass> dataList;
    private Context context;

    public PRecyclerAdapter(Context context, ArrayList<DataFasilitasClass> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public PMyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.p_recycler_item, parent, false);
        return new PRecyclerAdapter.PMyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PMyViewHolder holder, int position) {
        Glide.with(context).load(dataList.get(position).getImageUrl()).into(holder.recyclerImage);
        holder.recyclerNamaTempat.setText(dataList.get(position).getNamaTempat());
        holder.recyclerLokasi.setText(dataList.get(position).getLokasi());
        holder.recyclerJumlahAnggota.setText("Jumlah Anggota :" + dataList.get(position).getJumlahAnggota());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class PMyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView recyclerImage;
        TextView recyclerNamaTempat, recyclerLokasi, recyclerJumlahAnggota;

        public PMyViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerImage = itemView.findViewById(R.id.imageView);
            recyclerNamaTempat = itemView.findViewById(R.id.tvNamaTempat);
            recyclerLokasi = itemView.findViewById(R.id.tvLokasi);
            recyclerJumlahAnggota = itemView.findViewById(R.id.tvJumlahAnggota);
        }

        @Override
        public void onClick(View v) {
            //pindah halaman
        }
    }
}
