package com.example.mpsi5.admin.FragmentsAdmin.DataFragments.Data_FragmentAdmin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.DataRumah_masihSalah;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.VerifRecyclerAdapter_masihSalah;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TerferivikasiFragment_masihSalah extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<DataRumah_masihSalah> dataRumahList;
    private VerifRecyclerAdapter_masihSalah adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terferivikasi, container, false);

        //recycler
        recyclerView = view.findViewById(R.id.recyclerViewRumah);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//
        dataRumahList = new ArrayList<>();
        adapter = new VerifRecyclerAdapter_masihSalah(requireActivity(), dataRumahList);//
        recyclerView.setAdapter(adapter);

        DatabaseReference imagesRef = FirebaseDatabase.getInstance().getReference("User").child("Fasilitas");

        // Mendengarkan perubahan di direktori Images dan mengisi dataList
        imagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dataRumahList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataRumah_masihSalah dataClass = dataSnapshot.getValue(DataRumah_masihSalah.class);
//                    dataClass.setId(dataSnapshot.getKey());
                    dataRumahList.add(dataClass);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle any errors if necessary
            }
        });

        return view;
    }
}