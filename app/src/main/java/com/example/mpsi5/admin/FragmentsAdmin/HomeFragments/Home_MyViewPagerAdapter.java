package com.example.mpsi5.admin.FragmentsAdmin.HomeFragments;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Home_FragmetAdminMain.FasilitasUmumFragment;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Home_FragmetAdminMain.PemetaanFragment;

public class Home_MyViewPagerAdapter extends FragmentStateAdapter {
    public Home_MyViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new FasilitasUmumFragment();
            case 1:
                return new PemetaanFragment();
            default:
                return new FasilitasUmumFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
