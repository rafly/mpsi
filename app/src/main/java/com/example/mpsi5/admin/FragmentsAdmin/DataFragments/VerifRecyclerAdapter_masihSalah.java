package com.example.mpsi5.admin.FragmentsAdmin.DataFragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mpsi5.R;

import java.util.ArrayList;

public class VerifRecyclerAdapter_masihSalah extends RecyclerView.Adapter<VerifRecyclerAdapter_masihSalah.ViewHolderRumah> {

    private Context context;
    private ArrayList<DataRumah_masihSalah> dataRumahArrayList;

    public VerifRecyclerAdapter_masihSalah(Context context, ArrayList<DataRumah_masihSalah> dataRumahArrayList) {
        this.context = context;
        this.dataRumahArrayList = dataRumahArrayList;
    }

    @NonNull
    @Override
    public ViewHolderRumah onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.verif_rv_admin_item, parent, false);
        return new ViewHolderRumah(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRumah holder, int position) {
        DataRumah_masihSalah dataRumah = dataRumahArrayList.get(position);

        holder.textViewJalan.setText(dataRumah.getJalan());
        // Tambahan kode sesuai kebutuhan, misalnya untuk menampilkan gambar, dll.
    }

    @Override
    public int getItemCount() {
        return dataRumahArrayList.size();
    }

    public static class ViewHolderRumah extends RecyclerView.ViewHolder {
        TextView textViewJalan;
        ConstraintLayout parentLayout;

        public ViewHolderRumah(@NonNull View itemView) {
            super(itemView);
            textViewJalan = itemView.findViewById(R.id.verif_tvAlamat);
            parentLayout = itemView.findViewById(R.id.layoutVerif);
        }
    }
}
