package com.example.mpsi5.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mpsi5.LoginActivity;
import com.example.mpsi5.R;
import com.example.mpsi5.RegisterActivity;
import com.example.mpsi5.warga.WargaMainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AdminLoginActivity extends AppCompatActivity {

    Button btLogin;
    EditText loginEmail, loginPassword;
    TextView regis;
    private FirebaseAuth fireBaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        fireBaseAuth = FirebaseAuth.getInstance();
        btLogin = findViewById(R.id.btLogin_aToMain);
        loginEmail = findViewById(R.id.tietEmail_alogin);
        loginPassword = findViewById(R.id.tietPassword_alogin);
        regis = findViewById(R.id.toRegister4);

        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regis = new Intent(AdminLoginActivity.this, AdminRegisActivity.class);
                startActivity(regis);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = loginEmail.getText().toString();
                String password = loginPassword.getText().toString();
                login(email, password);
            }

            public void login(String email, String password) {
                fireBaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(AdminLoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(AdminLoginActivity.this, AdminMainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(AdminLoginActivity.this, "Maaf, email dan password Anda salah", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}