package com.example.mpsi5.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mpsi5.LoginActivity;
import com.example.mpsi5.R;
import com.example.mpsi5.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminRegisActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    EditText regisFullName, regisEmail, regisPassword;
    Button btRegis_toLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_regis);

        regisFullName = findViewById((R.id.tietNamaLengkap));
        regisEmail = findViewById(R.id.tietEmail_regis_admin);
        regisPassword = findViewById(R.id.tietPassword_regis_admin);
        btRegis_toLogin = findViewById(R.id.btLogin_ToMain_admin);

        firebaseAuth = FirebaseAuth.getInstance();

        btRegis_toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = regisEmail.getText().toString();
                String password = regisPassword.getText().toString();
                String fullName = regisFullName.getText().toString();
                registrasi(email, password, fullName);
            }
        });

    }

    private void registrasi(String email, String password, String fullName) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        // Simpan data pengguna ke Firebase Realtime Database
                        saveUserData(uid, fullName, email, password);
                    }
                    Intent toLogin = new Intent(AdminRegisActivity.this, AdminLoginActivity.class);
                    startActivity(toLogin);
                    finish();
                } else {
                    // Handle error jika registrasi gagal
                    Toast.makeText(AdminRegisActivity.this, "Registrasi gagal. Coba lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveUserData(String uid, String fullName, String email, String password) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Admin").child(uid);

        // Simpan data pengguna ke Firebase Realtime Database
        usersRef.child("fullName").setValue(fullName);
        usersRef.child("email").setValue(email);
        usersRef.child("password").setValue(password);
    }
}