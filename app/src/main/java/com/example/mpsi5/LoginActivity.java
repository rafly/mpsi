package com.example.mpsi5;

import static android.app.PendingIntent.getActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mpsi5.admin.AdminLoginActivity;
import com.example.mpsi5.admin.AdminMainActivity;
import com.example.mpsi5.warga.WargaMainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    TextView toRegiter, toLoginAdmin;
    Button btLogin;
    EditText loginEmail, loginPassword;
    private FirebaseAuth fireBaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fireBaseAuth = FirebaseAuth.getInstance();
        btLogin = findViewById(R.id.btLogin_ToMain);
        toRegiter = findViewById(R.id.toRegister);
        toLoginAdmin = findViewById(R.id.toLoginAdmin);
        loginEmail = findViewById(R.id.tietEmail_login);
        loginPassword = findViewById(R.id.tietPassword_login);

        toRegiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regis = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(regis);
            }
        });

        toLoginAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginAdmin = new Intent(LoginActivity.this, AdminLoginActivity.class);
                startActivity(loginAdmin);
            }
        });
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = loginEmail.getText().toString();
                String password = loginPassword.getText().toString();
                login(email, password);
            }

            public void login(String email, String password) {
                fireBaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(LoginActivity.this, WargaMainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Maaf, email dan password Anda salah", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}