package com.example.mpsi5.warga.FragmentsWarga.DataFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Info_FU;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Info_Data extends AppCompatActivity {

    Button btHapus;
    TextView nik, namaLengkap, ttl, jenisKelaminn, agama, jalan, rtrw, kelurahanDesa, kecamatan;
    String dataId;
    DatabaseReference databaseReference, databaseReferenceAlamat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_data);

        nik = findViewById(R.id.tvIsiNIK);
        namaLengkap = findViewById(R.id.tvIsiNamaLengkap);
        ttl = findViewById(R.id.tvIsiTTL);
        jenisKelaminn = findViewById(R.id.tvIsiJenisKelamin);
        agama = findViewById(R.id.tvIsiAgama);
        jalan = findViewById(R.id.tvIsiJalan);
        rtrw = findViewById(R.id.tvIsiRTRW);
        kelurahanDesa = findViewById(R.id.tvIsiKelurahanDesa);
        kecamatan = findViewById(R.id.tvIsiKecamatan);
        btHapus = findViewById(R.id.btHapusData);

        //Firebase
        FirebaseUser auth = FirebaseAuth.getInstance().getCurrentUser();
        String uid = auth.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Data").child("Warga").child(uid);

        Intent intent = getIntent();
        dataId = intent.getStringExtra("dataId");

        DatabaseReference databaseData = databaseReference.child(dataId);
        databaseData.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override

            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String nikData = snapshot.child("nik").getValue(String.class);
                    String namaLengkapData = snapshot.child("namaLengkap").getValue(String.class);
                    String ttlData = snapshot.child("ttl").getValue(String.class);
                    String jenisKelaminData = snapshot.child("jenisKelamin").getValue(String.class);
                    String agamaData = snapshot.child("agama").getValue(String.class);

                    nik.setText(nikData);
                    namaLengkap.setText(namaLengkapData);
                    ttl.setText(ttlData);
                    jenisKelaminn.setText(jenisKelaminData);
                    agama.setText(agamaData);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        databaseReferenceAlamat = FirebaseDatabase.getInstance().getReference("User").child(uid);
        databaseReferenceAlamat.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    String jalanData = dataSnapshot.child("jalan").getValue(String.class);
                    String rtrwData = dataSnapshot.child("rtrw").getValue(String.class);
                    String kelurahanDesaData = dataSnapshot.child("kelurahanDesa").getValue(String.class);
                    String kecamatanData = dataSnapshot.child("kecamatan").getValue(String.class);

                    jalan.setText(jalanData);
                    rtrw.setText(rtrwData);
                    kelurahanDesa.setText(kelurahanDesaData);
                    kecamatan.setText(kecamatanData);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        btHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Call a method to delete the data
                showDeleteConfirmationPopup();
            }
        });
    }
    private void showDeleteConfirmationPopup() {
        // Inflate the popup layout
        View popupView = getLayoutInflater().inflate(R.layout.delete_warga_popup, null);

        // Create a PopupWindow
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        // Add a semi-transparent background
        View backgroundView = new View(this);
        backgroundView.setBackgroundColor(Color.parseColor("#80000000")); // 80% black
        backgroundView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        ViewGroup rootView = (ViewGroup) getWindow().getDecorView().getRootView();
        rootView.addView(backgroundView);

        // Set up UI elements and button click events in the popup
        ImageButton closeButton = popupView.findViewById(R.id.imageButton4);
        Button deleteButton = popupView.findViewById(R.id.btDealHapus);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close the popup and remove the semi-transparent background
                rootView.removeView(backgroundView);
                popupWindow.dismiss();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDataFromDatabase();
                // Call a method to handle the deletion of data
                // TODO: Add your data deletion logic here

                // Close the popup and remove the semi-transparent background
                rootView.removeView(backgroundView);
                popupWindow.dismiss();
            }
        });

        // Show the popup centered on the screen
        popupWindow.showAtLocation(btHapus, Gravity.CENTER, 0, 0);
    }
    private void deleteDataFromDatabase() {
        databaseReference.child(dataId).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Data successfully deleted
                        Log.d("Info_Data", "Data successfully deleted");
                        finish(); // Close the activity after deletion
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Handle the failure to delete data
                        Log.w("Info_Data", "Error deleting data", e);
                        // You can show an error message or handle it as needed
                    }
                });
    }
}