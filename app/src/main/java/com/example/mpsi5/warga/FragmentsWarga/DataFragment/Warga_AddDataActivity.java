package com.example.mpsi5.warga.FragmentsWarga.DataFragment;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.AdminMainActivity;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.DataWargaClass;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.AdminFUBaruActivity;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.DataFasilitasClass;
import com.example.mpsi5.warga.WargaMainActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Warga_AddDataActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    EditText namaLengkap, TTL, NIK, agama;
    Button uploadKeluarga;
    ImageButton btBack, gambar;
    private Uri imageUri;
    //    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    final private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    String[] itemsJenisKelamin = {"Laki-laki", "Perempuan"};
    String[] itemsRelasi = {"Kepala Keluarga", "Istri", "Anak"};
    AutoCompleteTextView jenisKelamin, relasi;
    ArrayAdapter<String> adapterJenisKelamin, adapterRelasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warga_add_data);

        namaLengkap = findViewById(R.id.tietNamaLengkap);
        TTL = findViewById(R.id.tietTTL);
        NIK = findViewById(R.id.tietNIK);
        agama = findViewById(R.id.tietAgama);
//        alamat = findViewById(R.id.tietAlamat);
        uploadKeluarga = findViewById(R.id.buttonUploadBangunan);
        gambar = findViewById(R.id.imageButtonGambar);
        btBack = findViewById(R.id.btBack);
        jenisKelamin = findViewById(R.id.actvJenisKelamin);
        relasi = findViewById(R.id.actvRelasi);

        adapterJenisKelamin = new ArrayAdapter<String>(this,R.layout.dropdown_item, itemsJenisKelamin);
        adapterRelasi= new ArrayAdapter<String>(this,R.layout.dropdown_item, itemsRelasi);
        jenisKelamin.setAdapter(adapterJenisKelamin);
        relasi.setAdapter(adapterRelasi);

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            imageUri = data.getData();
                            gambar.setImageURI(imageUri);
                        } else {
                            Toast.makeText(Warga_AddDataActivity.this, "No Image Selected", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPicker = new Intent();
                photoPicker.setAction(Intent.ACTION_GET_CONTENT);
                photoPicker.setType("image/*");
                activityResultLauncher.launch(photoPicker);
            }
        });

        uploadKeluarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageUri != null && !TextUtils.isEmpty(namaLengkap.getText()) && !TextUtils.isEmpty(TTL.getText()) &&
                        !TextUtils.isEmpty(NIK.getText()) && !TextUtils.isEmpty(agama.getText()) &&
                        !TextUtils.isEmpty(jenisKelamin.getText()) && !TextUtils.isEmpty(relasi.getText())) {
                    uploadToFirebase(imageUri,namaLengkap,TTL,NIK,agama,jenisKelamin,relasi);
                } else {
                    Toast.makeText(Warga_AddDataActivity.this, "Isi semua data untuk bisa mengunggah!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void uploadToFirebase(Uri uri, EditText namaLengkap, EditText ttl, EditText nik, EditText agama, AutoCompleteTextView jenisKelamin, AutoCompleteTextView relasi) {

        String dNamaLengkap = namaLengkap.getText().toString();
        String dNIK = nik.getText().toString();
        String dTTL = ttl.getText().toString();
//        String dAlamat = alamat.getText().toString();
        String dAgama = agama.getText().toString();
        String dJenisKelamin = jenisKelamin.getText().toString();
        String dRelasi = relasi.getText().toString();
        String status = "blmTerferivikasi";

        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        String uid = currentUser.getUid();

        String uniqueFileName = System.currentTimeMillis() + "." + getFileExtension(uri);
        final StorageReference imageReference = storageReference.child(uid).child(uniqueFileName);



        // Check if the file with the same name already exists
        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // If onSuccess is called, the file with the same name already exists
                // Handle the case here, you may want to show an error message or take appropriate action
                Toast.makeText(Warga_AddDataActivity.this, "File with the same name already exists", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                // If onFailure is called, the file doesn't exist, so you can proceed with the upload
                imageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                DataWargaClass dataWargaClass = new DataWargaClass(uri.toString(), dNamaLengkap, dTTL, dNIK, dJenisKelamin, dAgama, dRelasi, status);
                                String keyPerOrang = databaseReference.child("Data").child("Warga").child(uid).push().getKey();
                                databaseReference.child("Data").child("Warga").child(uid).child(keyPerOrang).setValue(dataWargaClass);
                                Toast.makeText(Warga_AddDataActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Warga_AddDataActivity.this, WargaMainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Warga_AddDataActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private String getFileExtension(Uri fileUri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(fileUri));
    }

}