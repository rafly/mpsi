package com.example.mpsi5.warga.FragmentsWarga;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.DataFasilitasClass;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.FURecyclerAdapter;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Home_FragmetAdminMain.FasilitasUmumFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class WargaHomeFragment extends Fragment {
    FasilitasUmumFragment fasilitasUmumFragment;
    private RecyclerView recyclerView;
    private ArrayList<DataFasilitasClass> dataList;
    private FURecyclerAdapter adapter;
    private SearchView searchBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_warga_home, container, false);

        searchBar = view.findViewById(R.id.search_bar_warga);
        searchBar.clearFocus();

        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterList(s);
                return false;
            }
        });
        //recycler
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//
        dataList = new ArrayList<>();
        adapter = new FURecyclerAdapter(requireActivity(), dataList);//
        recyclerView.setAdapter(adapter);

        DatabaseReference imagesRef = FirebaseDatabase.getInstance().getReference("Data").child("Fasilitas");

        // Mendengarkan perubahan di direktori Images dan mengisi dataList
        imagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dataList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    DataFasilitasClass dataClass = dataSnapshot.getValue(DataFasilitasClass.class);
                    dataClass.setId(dataSnapshot.getKey());
                    dataList.add(dataClass);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle any errors if necessary
            }
        });
        return view;
    }

    private void filterList(String s) {
        ArrayList<DataFasilitasClass> filteredListLoc = new ArrayList<>();
        for (DataFasilitasClass dataFasilitasClass : dataList) {
            if (dataFasilitasClass.getLokasi().toLowerCase().contains(s.toLowerCase())) {
                filteredListLoc.add(dataFasilitasClass);
            }
        }
        if (filteredListLoc.isEmpty()) {
            Toast.makeText(requireContext(), "Lokasi Tidak Ditemukan", Toast.LENGTH_SHORT).show();
        } else {
            adapter.setFilteredList(filteredListLoc);
        }
    }

}