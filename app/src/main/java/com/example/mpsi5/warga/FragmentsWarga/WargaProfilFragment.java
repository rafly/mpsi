package com.example.mpsi5.warga.FragmentsWarga;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.mpsi5.LoginActivity;
import com.example.mpsi5.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.nio.charset.StandardCharsets;

public class WargaProfilFragment extends Fragment {

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    TextView namaLengkap, email, password, keluar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_warga_profil, container, false);

        namaLengkap = view.findViewById(R.id.namaLengkapWarga);
        email = view.findViewById(R.id.emailWarga);
        password = view.findViewById(R.id.passwordWarga);
        keluar = view.findViewById(R.id.tvKeluar);

        // Set user's full name and email to TextViews
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("User").child(currentUser.getUid());

        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String fullName = dataSnapshot.child("fullName").getValue(String.class);
                String userEmail = dataSnapshot.child("email").getValue(String.class);
                String userPassword = dataSnapshot.child("password").getValue(String.class);

                if (fullName != null) {
                    namaLengkap.setText(fullName);
                }

                if (userEmail != null) {
                    email.setText(userEmail);
                }

                if (userPassword != null) {
                    password.setText(userPassword);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error jika terjadi kesalahan saat mengambil data dari Firebase
            }
        });

        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();

                Intent toLogin = new Intent(requireContext(), LoginActivity.class);
                startActivity(toLogin);
                requireActivity().finish();
            }
        });

        return view;
    }
}
