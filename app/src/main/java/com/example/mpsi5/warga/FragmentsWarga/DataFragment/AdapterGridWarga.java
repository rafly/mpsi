package com.example.mpsi5.warga.FragmentsWarga.DataFragment;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.DataWargaClass;
import com.example.mpsi5.admin.FragmentsAdmin.HomeFragments.Info_FU;

import java.util.ArrayList;

public class AdapterGridWarga extends BaseAdapter {
    private ArrayList<DataWargaClass> dataListWarga;
    private Context context;
    LayoutInflater layoutInflater;

    public AdapterGridWarga(ArrayList<DataWargaClass> dataListWarga, Context context){

        this.context = context;
        this.dataListWarga = dataListWarga;

    }
    @Override
    public int getCount() {
        return dataListWarga.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.grid_item, null);
        }

        TextView gridNamaLengkap = view.findViewById(R.id.tvNamaLengkap);
        TextView  gridNIK = view.findViewById(R.id.tvNIK);

        gridNamaLengkap.setText(dataListWarga.get(i).getNamaLengkap());
        gridNIK.setText(dataListWarga.get(i).getNIK());
        ConstraintLayout layoutWarna1 = view.findViewById(R.id.layoutWarna1);
        CardView cardViewHuman = view.findViewById(R.id.cardViewHuman);
        ImageView imageView5 = view.findViewById(R.id.imageView5);
        ImageView ivStatusData = view.findViewById(R.id.ivStatusData);

        String relasi = dataListWarga.get(i).getRelasi();
        if (relasi != null && relasi.equals("Kepala Keluarga")) {
            layoutWarna1.setBackgroundColor(ContextCompat.getColor(context, R.color.sky_blue));
            cardViewHuman.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
            imageView5.setImageResource(R.drawable.baseline_person_blue_24);
            ivStatusData.setImageResource(R.drawable.baseline_verified_white_24);
        }

        final int currentPosition = i;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Panggil metode atau lakukan tindakan yang diperlukan saat item di klik
                Intent intent = new Intent(context, Info_Data.class);
                dataListWarga.forEach(dataWarga -> {
                    Log.d("data warga", dataWarga.toString());
                });
//                intent.putExtra("dataId", dataListWarga.get(currentPosition).getId());
                intent.putExtra("dataId", dataListWarga.get(currentPosition).getId());
                context.startActivity(intent);
            }
        });

        return view;
    }
}
