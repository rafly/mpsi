package com.example.mpsi5.warga.FragmentsWarga.DataFragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.mpsi5.R;
import com.example.mpsi5.admin.FragmentsAdmin.DataFragments.DataWargaClass;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class WargaDataFragment extends Fragment {

    FloatingActionButton floatingActionButton;
    GridView data;
    ArrayList<DataWargaClass> dataListWarga;
    AdapterGridWarga adapter;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser user = auth.getCurrentUser();
    String currentUserUID = user.getUid();
    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Data").child("Warga").child(currentUserUID);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_warga_data, container, false);

        data = view.findViewById(R.id.gridWarga);
        floatingActionButton = view.findViewById(R.id.floatingActionButton3);

        dataListWarga = new ArrayList<>();
        adapter = new AdapterGridWarga(dataListWarga, getActivity());
        data.setAdapter(adapter);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Warga_AddDataActivity.class);
                startActivity(intent);
            }
        });

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                dataListWarga.clear(); // Hapus data sebelum menambahkan yang baru
                for (DataSnapshot dataWargaSnapshot : snapshot.getChildren()) {
                    // Jika setiap anak adalah data warga di dalam BlokA
                    String imageURL = dataWargaSnapshot.child("imageURL").getValue(String.class);
                    String namaLengkap = dataWargaSnapshot.child("namaLengkap").getValue(String.class);
                    String TTL = dataWargaSnapshot.child("TTL").getValue(String.class);
                    String nik = dataWargaSnapshot.child("nik").getValue(String.class);
                    String jenisKelamin = dataWargaSnapshot.child("jenisKelamin").getValue(String.class);
                    String agama = dataWargaSnapshot.child("agama").getValue(String.class);
                    String relasi = dataWargaSnapshot.child("relasi").getValue(String.class);
                    String status = dataWargaSnapshot.child("statusAkun").getValue(String.class);


                    // Tambahkan data ke dataListWarga
                    DataWargaClass dataWarga = new DataWargaClass(dataWargaSnapshot.getKey(), imageURL, namaLengkap, TTL, nik, jenisKelamin, agama, relasi, status );
                    dataWarga.setNamaLengkap(namaLengkap);
                    dataWarga.setNIK("NIK. "+ nik);
                    dataListWarga.add(dataWarga);

                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Tangani kesalahan
                Log.e("WargaDataFragment", "Error reading data from database", error.toException());
            }
        });

        return view;
    }
}
