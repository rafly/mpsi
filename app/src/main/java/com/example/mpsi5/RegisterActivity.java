package com.example.mpsi5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    EditText regisFullName, regisEmail, regisPassword, regisJalan, regisRtRw, regisKelurahanDesa, regisKecamatan;
    Button btRegis_toLogin;
    TextView hintFullName, hintEmail, hintPassword, hintJalan, hintRtRw, hintKelurahanDesa, hintKecamatan;
    Switch switch1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        regisFullName = findViewById((R.id.tietFullName_regis));
        regisEmail = findViewById(R.id.tietEmail_regis);
        regisPassword = findViewById(R.id.tietPassword_regis);
        regisJalan = findViewById(R.id.tietJalan);
        regisRtRw = findViewById(R.id.tietRtRw);
        regisKelurahanDesa = findViewById(R.id.tietKelurahanDesa);
        regisKecamatan = findViewById(R.id.tietKecamatan);

        btRegis_toLogin = findViewById(R.id.btRegis_ToLogin);
        hintEmail = findViewById(R.id.tvHintEmail);
        hintPassword = findViewById(R.id.tvHintPassword);
        hintFullName = findViewById(R.id.tvHintFullName);
        hintJalan = findViewById(R.id.tvHintJalan);
        hintRtRw = findViewById(R.id.tvHintRtRw);
        hintKelurahanDesa = findViewById(R.id.tvKelurahanDesa);
        hintKecamatan = findViewById(R.id.tvHintKecamatan);

        switch1 = findViewById(R.id.switch1);
        firebaseAuth = FirebaseAuth.getInstance();

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Update the button status when the switch state changes
                updateLoginButtonStatus();
            }
        });

        btRegis_toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInputs()) {
                    String email = regisEmail.getText().toString();
                    String password = regisPassword.getText().toString();
                    String fullName = regisFullName.getText().toString();
                    String jalan = regisJalan.getText().toString();
                    String rtrw = regisRtRw.getText().toString();
                    String kelurahanDesa = regisKelurahanDesa.getText().toString();
                    String kecamatan = regisKecamatan.getText().toString();
                    registrasi(email, password, fullName, jalan, rtrw, kelurahanDesa, kecamatan);
                }
            }
        });
    }

    private void updateLoginButtonStatus() {
        // Update the button status based on switch state and input validations
        Button btRegis_toLogin = findViewById(R.id.btRegis_ToLogin);
        btRegis_toLogin.setEnabled(validateInputs());
    }

    private boolean validateInputs() {
        boolean isValid = true;

        String fullName = regisFullName.getText().toString();
        String email = regisEmail.getText().toString();
        String password = regisPassword.getText().toString();
        String jalan = regisJalan.getText().toString();
        String rtrw = regisRtRw.getText().toString();
        String kelurahanDesa = regisKelurahanDesa.getText().toString();
        String kecamatan = regisKecamatan.getText().toString();

        // Validasi switch1
        if (!switch1.isChecked()) {
            Toast.makeText(RegisterActivity.this, "Anda harus menyetujui kebijakan dan persyaratan", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        // Validasi kolom nama lengkap
        if (fullName.isEmpty()) {
            hintFullName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintFullName.setVisibility(View.INVISIBLE);
        }

        // Validasi kolom email
        if (email.isEmpty()) {
            hintEmail.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintEmail.setVisibility(View.INVISIBLE);
        }

        // Validasi kolom password
        if (password.isEmpty()) {
            hintPassword.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintPassword.setVisibility(View.INVISIBLE);
        }

        //Validasi kolom jalan
        if (jalan.isEmpty()) {
            hintJalan.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintJalan.setVisibility(View.INVISIBLE);
        }

        //Validasi kolom rtrw
        if (rtrw.isEmpty()) {
            hintRtRw.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintRtRw.setVisibility(View.INVISIBLE);
        }

        //Validasi kolom kelurahanDesa
        if (kelurahanDesa.isEmpty()) {
            hintKelurahanDesa.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintKelurahanDesa.setVisibility(View.INVISIBLE);
        }

        //Validasi kolom kecamatan
        if (kecamatan.isEmpty()) {
            hintKecamatan.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            hintKecamatan.setVisibility(View.INVISIBLE);
        }

        // Aktifkan atau nonaktifkan tombol pendaftaran
        return isValid && switch1.isChecked();
    }

    private void registrasi(String email, String password, String fullName, String jalan, String rtrw, String kelurahanDesa, String kecamatan) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        // Simpan data pengguna ke Firebase Realtime Database
                        saveUserData(uid, fullName, email, password, jalan, rtrw, kelurahanDesa, kecamatan);
                    }
                    Intent toLogin = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(toLogin);
                    finish();
                } else {
                    // Handle error jika registrasi gagal
                    Toast.makeText(RegisterActivity.this, "Registrasi gagal. Coba lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveUserData(String uid, String fullName, String email, String password, String jalan, String rtrw, String kelurahanDesa, String kecamatan) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("User").child(uid);

        // Simpan data pengguna ke Firebase Realtime Database
        usersRef.child("fullName").setValue(fullName);
        usersRef.child("email").setValue(email);
        usersRef.child("password").setValue(password);
        usersRef.child("jalan").setValue(jalan);
        usersRef.child("rtrw").setValue(rtrw);
        usersRef.child("kelurahanDesa").setValue(kelurahanDesa);
        usersRef.child("kecamatan").setValue(kecamatan);
    }
}
